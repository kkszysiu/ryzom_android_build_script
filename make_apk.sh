#!/bin/sh
# Copyright (C) 2012 Krzysztof Klinikowski <kkszysiu@gmail.com>

LOCALDIR=`pwd $0`

SDK_PATH="/Users/kkszysiu/Library/Android/sdk/"
ANDROID_PLATFORM="android-21"

platform="unknown"
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  platform='linux'
elif [[ "$OSTYPE" == "darwin"* ]]; then
  platform='darwin'
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  platform="unknown"
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  platform="unknown"
elif [[ "$OSTYPE" == "win32" ]]; then
  # I'm not sure this can happen.
  platform="unknown"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  platform="unknown"
else
  platform="unknown"
fi

PROJECT_NAME="Ryzom-debug"
PROJECT_PATH="ryzom_android_project"

CP_LIBS_FROM="./ryzom_androidedition/lib/"
PROJECT_LIBS_PATH="./$PROJECT_PATH/libs/armeabi-v7a/"

rm -rf ./$PROJECT_PATH/bin/
#rm -rf $PROJECT_LIBS_PATH/

mkdir -p ./$PROJECT_PATH/libs/armeabi-v7a/

cp -f ./nativeandroidtoolchain/sysroot/usr/lib/libfreetype2.so $PROJECT_LIBS_PATH/libfreetype2.so
./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libfreetype2.so
cp -f ./nativeandroidtoolchain/sysroot/usr/lib/libcurl.so $PROJECT_LIBS_PATH/libcurl.so
./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libcurl.so

cp -f $CP_LIBS_FROM/libryzom_client.so $PROJECT_LIBS_PATH/libryzom_client.so
./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libryzom_client.so

#cp $CP_LIBS_FROM/libryzom_clientsheets.so.0.8.0 $PROJECT_LIBS_PATH/libryzom_clientsheets.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libryzom_clientsheets.so
#cp $CP_LIBS_FROM/libryzom_client.so.0.8.0 $PROJECT_LIBS_PATH/libryzom_client.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libryzom_client.so
#cp $CP_LIBS_FROM/libryzom_gameshare.so.0.8.0 $PROJECT_LIBS_PATH/libryzom_gameshare.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libryzom_gameshare.so
#cp $CP_LIBS_FROM/libnelpacs.so.0.8.0 $PROJECT_LIBS_PATH/libnelpacs.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelpacs.so
#cp $CP_LIBS_FROM/libnelsnd_lowlevel.so.0.8.0 $PROJECT_LIBS_PATH/libnelsnd_lowlevel.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelsnd_lowlevel.so
#cp $CP_LIBS_FROM/libnelsound.so.0.8.0 $PROJECT_LIBS_PATH/libnelsound.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelsound.so
#cp $CP_LIBS_FROM/libnelnet.so.0.8.0 $PROJECT_LIBS_PATH/libnelnet.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelnet.so
#cp $CP_LIBS_FROM/libnellogic.so.0.8.0 $PROJECT_LIBS_PATH/libnellogic.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnellogic.so
#cp $CP_LIBS_FROM/libnelligo.so.0.8.0 $PROJECT_LIBS_PATH/libnelligo.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelligo.so
#cp $CP_LIBS_FROM/libnelgeorges.so.0.8.0 $PROJECT_LIBS_PATH/libnelgeorges.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelgeorges.so
#cp $CP_LIBS_FROM/libnel3d.so.0.8.0 $PROJECT_LIBS_PATH/libnel3d.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnel3d.so
#cp $CP_LIBS_FROM/libnelmisc.so.0.8.0 $PROJECT_LIBS_PATH/libnelmisc.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libnelmisc.so
#cp $CP_LIBS_FROM/libryzom_sevenzip.so.0.8.0 $PROJECT_LIBS_PATH/libryzom_sevenzip.so
#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all $PROJECT_LIBS_PATH/libryzom_sevenzip.so

cp ./ryzom_androidedition/nel/libnel_drv_opengles.so $PROJECT_LIBS_PATH/libnel_drv_opengles.so
cp ./ryzom_androidedition/nel/libnel_drv_opengles.so $PROJECT_LIBS_PATH/libnel_drv_opengl.so

if [ "$platform" = "darwin" ]; then
  sed -i '' -e 's|sdk.dir=.*|sdk.dir='${SDK_PATH}'|g' $LOCALDIR/ryzom_android_project/local.properties
  sed -i '' -e 's|target=.*|target='${ANDROID_PLATFORM}'|g' $LOCALDIR/ryzom_android_project/project.properties
else
  sed -i 's|sdk.dir=.*|sdk.dir='${SDK_PATH}'|g' $LOCALDIR/ryzom_android_project/local.properties
  sed -i 's|target=.*|target='${ANDROID_PLATFORM}'|g' $LOCALDIR/ryzom_android_project/project.properties
fi

project.properties

cd ./$PROJECT_PATH
ant debug
cd ..
adb uninstall com.kkszysiu.ryzom
adb install -r ./$PROJECT_PATH/bin/$PROJECT_NAME.apk

adb shell am start -n com.kkszysiu.ryzom/com.kkszysiu.ryzom.GameActivity
