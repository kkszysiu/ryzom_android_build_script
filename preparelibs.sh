#!/bin/bash
# Copyright 2012 Krzysztof Klinikowski <kkszysiu@gmail.com>


find ./ryzom_androidedition/ -type l -delete;

for i in `ls ./ryzom_androidedition/lib/ | grep "\.so"`
do
	if [ "${i:${#i}-9}" == ".so.0.8.0" ]; then
		mv ./ryzom_androidedition/lib/$i ./ryzom_androidedition/lib/${i::-4}
		#mv ./ryzom_androidedition/lib/$i ./ryzom_androidedition/lib/${i::-2}
		echo "Executing: mv ./ryzom_androidedition/lib/$i ./ryzom_androidedition/lib/${i::-2}"
		#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all  ./ryzom_androidedition/lib/${i::-4}
		#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all  ./ryzom_androidedition/lib/${i::-2}
	fi
done

#adb push ./ryzom_androidedition/lib/ /data/local/tmp/ryzom_android/lib/

#adb push ./ryzom_androidedition/nel/libnel_drv_opengles.so /data/local/tmp/ryzom_android/lib/libnel_drv_opengles.so

#./nativeandroidtoolchain/bin/arm-linux-androideabi-strip --strip-all ./ryzom_androidedition/games/ryzom_client
#adb push ./ryzom_androidedition/games/ /data/local/tmp/ryzom_android/games/


#ryzom_androidedition/lib
#mv libnel3d.so.0.8.0 libnel3d.so.0.8
