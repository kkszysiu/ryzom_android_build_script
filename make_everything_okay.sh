set -e

LOCALDIR=`pwd $0`

cd $LOCALDIR/ryzomcore/code/build/
make install -j8
cd $LOCALDIR

./make_apk.sh
