#!/bin/bash
# Copyright 2012-2013 Krzysztof Klinikowski <kkszysiu@gmail.com>

LOCALDIR=`pwd $0`

platform="unknown"
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  platform='linux'
elif [[ "$OSTYPE" == "darwin"* ]]; then
  platform='darwin'
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  platform="unknown"
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  platform="unknown"
elif [[ "$OSTYPE" == "win32" ]]; then
  # I'm not sure this can happen.
  platform="unknown"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  platform="unknown"
else
  platform="unknown"
fi

TOOLCHAIN=$LOCALDIR/nativeandroidtoolchain/
export TOOLCHAIN=$TOOLCHAIN
export ARCH=armv7-a
SYSROOT=$TOOLCHAIN/sysroot
echo "SYSROOT: $SYSROOT"
export SYSROOT=$SYSROOT
export PATH=$PATH:$NDK:$TOOLCHAIN/bin:$SYSROOT/usr/local/bin

RYHOME="$LOCALDIR/ryzomcore/code"
export RYHOME=$RYHOME

DESTPATH=$LOCALDIR/ryzom_androidedition/
export DESTPATH=$DESTPATH

#NDK=$LOCALDIR/android-ndk-r8/
#export ANDROID_NDK=$NDK
export ANDROID_STANDALONE_TOOLCHAIN=$TOOLCHAIN

IS_DOWNLOADED=`ls -l | grep "^d" | grep "ryzomcore"`
if [ -z "$IS_DOWNLOADED" ]; then
    hg clone https://kkszysiu@bitbucket.org/kkszysiu/ryzomcore
else
    echo "ryzomcore sources are already downloaded..."
fi

cd $RYHOME
#rm -rf $RYHOME/build # for testing purposes only!!!
mkdir -p $RYHOME/build
cd $RYHOME/build

if [ "$platform" = "darwin" ]; then
  sed -i '' -e 's/FIND_PACKAGE(Threads REQUIRED)/#FIND_PACKAGE(Threads REQUIRED)/g' $RYHOME/CMakeLists.txt
else
  sed -i 's/FIND_PACKAGE(Threads REQUIRED)/#FIND_PACKAGE(Threads REQUIRED)/g' $RYHOME/CMakeLists.txt
fi

#OPENGLES_GLES_LIBRARY=""
#OPENGLES_EGL_LIBRARY=""
# -DOPENGLES_GLES_LIBRARY=$OPENGLES_GLES_LIBRARY -DOPENGLES_EGL_LIBRARY=$OPENGLES_EGL_LIBRARY
#-DWITH_NEL_SAMPLES=ON

cmake -DCMAKE_TOOLCHAIN_FILE=$LOCALDIR/android.toolchain.cmake -DANDROID_STANDALONE_TOOLCHAIN=$TOOLCHAIN \
      -DBUILD_WITH_STANDALONE_TOOLCHAIN=ON -DWITH_LIBWWW_STATIC=ON -DCMAKE_BUILD_TYPE=Release \
      -DWITH_STATIC=ON -DTARGET_CPU="arm" -DWITH_SYMBOLS=ON -DFINAL_VERSION=OFF -DWITH_NEL_SAMPLES=OFF -DWITH_NEL_TOOLS=OFF \
      -DWITH_NEL_TESTS=OFF -DWITH_RYZOM_TOOLS=OFF -DWITH_RYZOM_SERVER=OFF -DWITH_RYZOM_CLIENT=ON -DWITH_NEL=ON -DWITH_SOUND=ON \
      -DWITH_SSE3=OFF -DWITH_SSE2=OFF \
      -DWITH_DRIVER_OPENAL=OFF -DWITH_DRIVER_OPENGL=OFF -DWITH_DRIVER_OPENGLES=ON -DRYZOM_PREFIX=$DESTPATH \
      -DVORBIS_INCLUDE_DIR=$SYSROOT/usr/include/ -DVORBIS_LIBRARY=$SYSROOT/usr/lib/libvorbisidec.a -DVORBISFILE_LIBRARY=$SYSROOT/usr/lib/libvorbisidec.a \
      -DCMAKE_INSTALL_PREFIX=$DESTPATH -DRYZOM_BIN_PREFIX=$DESTPATH/bin -DRYZOM_ETC_PREFIX=$DESTPATH/etc/ryzom \
      -DRYZOM_GAMES_PREFIX=$DESTPATH/games -DRYZOM_SBIN_PREFIX=$DESTPATH/sbin -DRYZOM_SHARE_PREFIX=$DESTPATH/share/ryzom \
      -DNL_BIN_PREFIX=$DESTPATH/bin -DNL_DRIVER_PREFIX=$DESTPATH/nel -DNL_ETC_PREFIX=$DESTPATH/etc/ -DNL_LIB_PREFIX=$DESTPATH/lib/ \
      -DNL_SBIN_PREFIX=$DESTPATH/sbin -DNL_SHARE_PREFIX=$DESTPATH/share/nel -DPLATFORM_ROOT=$SYSROOT ..

VERBOSE=1 make -j4
# make -j4

cd $LOCALDIR

#echo "Copying gnustl_static.a"
#cp $LOCALDIR/android-ndk-r8d/sources/cxx-stl/gnu-libstdc++/libs/armeabi-v7a/libgnustl_static.a $RYHOME/build/CMakeFiles/ndklibs/armeabi-v7a/libgnustl_static.a


echo "Copying libfreetype2"
cp $LOCALDIR/nativeandroidtoolchain/sysroot/usr/lib/libfreetype.so $RYHOME/build/CMakeFiles/ndklibs/armeabi-v7a/libfreetype.so

#this removes symbolic links, they cannot be copied into android
# find ./ryzom_androidedition/ -type l -delete;

# this is looking for symbolic links
# find ./ryzom_androidedition/ -type l -exec ls -lad {} \;
