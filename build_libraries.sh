
#!/bin/bash
# Copyright 2012-2013 Krzysztof Klinikowski <kkszysiu@gmail.com>

BUILD_OPENSSL=false
BUILD_CURL=false
BUILD_LUA=false
BUILD_BOOST=false
BUILD_LUABIND=false
BUILD_FREETYPE=false
BUILD_OGG=false
BUILD_TREMOR=true
BUILD_XML=false
BUILD_PNG=false
BUILD_WWW=false

#set -e
platform="unknown"
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  platform='linux'
elif [[ "$OSTYPE" == "darwin"* ]]; then
  platform='darwin'
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  platform="unknown"
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  platform="unknown"
elif [[ "$OSTYPE" == "win32" ]]; then
  # I'm not sure this can happen.
  platform="unknown"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  platform="unknown"
else
  platform="unknown"
fi

PARALLEL_JOBS="-j6"
CROSS_COMPILE_PREFIX=arm-linux-androideabi
ANDROID_PLATFORM="android-21" # not used anywhere yet

IS_DOWNLOADED=`ls -l | grep "android-ndk-r10e-"$platform"-x86_64.bin"`
if [ -z "$IS_DOWNLOADED" ]; then
    curl "http://dl.google.com/android/ndk/android-ndk-r10e-"$platform"-x86_64.bin" -o "android-ndk-r10e-"$platform"-x86_64.bin"
else
    echo "NDK is already downloaded..."
fi

ALREADY_UNPACKED=`ls -l | grep "^d" | grep "android-ndk-r10e"`
if [ -z "$ALREADY_UNPACKED" ]; then
    chmod a+x android-ndk-r10e-$platform-x86_64.bin
    ./android-ndk-r10e-$platform-x86_64.bin
else
    echo "NDK seems to be already unpacked..."
fi

LOCALDIR=`pwd $0`

ALREADY_PREPARED=`ls -l | grep "^d" | grep "nativeandroidtoolchain"`
if [ -z "$ALREADY_PREPARED" ]; then
    cd android-ndk-r10e
    ./build/tools/make-standalone-toolchain.sh --platform=$ANDROID_PLATFORM --arch=arm --toolchain=arm-linux-androideabi-clang3.6 --install-dir=$LOCALDIR/nativeandroidtoolchain/
    cd ..
else
    echo "Standalone toolchain seems to be already prepared..."
fi

export HOSTCONF=arm-eabi-linux
NDK=$LOCALDIR/android-ndk-r10e/

export NDK=$NDK
export AndroidNDKRoot=$NDK
export ANDROID_PLATFORM=$ANDROID_PLATFORM

export TOOLCHAIN=$LOCALDIR/nativeandroidtoolchain/
export ARCH=armv7-a
SYSROOT=$TOOLCHAIN/sysroot
export SYSROOT=$SYSROOT
export PATH=$PATH:$NDK:$TOOLCHAIN/bin:$SYSROOT/usr/local/bin
echo "PATH: export PATH=$PATH"
export CC=${CROSS_COMPILE_PREFIX}-clang
export CXX=${CROSS_COMPILE_PREFIX}-clang++
export LINK=${CXX}
export AR=${CROSS_COMPILE_PREFIX}-ar
export AS=${CROSS_COMPILE_PREFIX}-as
export LD=${CROSS_COMPILE_PREFIX}-ld
export RANLIB=${CROSS_COMPILE_PREFIX}-ranlib
export NM=${CROSS_COMPILE_PREFIX}-nm
export CFLAGS="-DANDROID -mandroid -fomit-frame-pointer --sysroot $SYSROOT -march=$ARCH -mfloat-abi=softfp -mfpu=vfp -mthumb -I${SYSROOT}/usr/include/"
export CXXFLAGS="$CFLAGS"
export LDLIBS="${NDK}/sources/cxx-stl/gnu-libstdc++/4.9/libs/armeabi-v7a/ -lgnustl_static"
export LDFLAGS="-L${NDK}/sources/cxx-stl/gnu-libstdc++/4.9/libs/armeabi-v7a/ -L${SYSROOT}/usr/lib/ -landroid"

#Building with TOOLSET=gcc-androidR8 CXXPATH=/home/krzysiek/Pobrane/android-ndk-r8//toolchains/arm-linux-androideabi-4.4.3/prebuilt/linux-x86/bin/arm-linux-androideabi-g++ CXXFLAGS=-I/home/krzysiek/Pobrane/android-ndk-r8//platforms/android-9/arch-arm/usr/include -I/home/krzysiek/Pobrane/android-ndk-r8//sources/cxx-stl/gnu-libstdc++/include -I/home/krzysiek/Pobrane/android-ndk-r8//sources/cxx-stl/gnu-libstdc++/libs/armeabi/include

echo "================================================"
echo "= Starting download and compilation of openssl ="
echo "================================================"

if $BUILD_OPENSSL; then
    IS_DOWNLOADED=`ls -l | grep "openssl-1.0.1c.tar.gz"`
    if [ -z "$IS_DOWNLOADED" ]; then
        curl http://www.openssl.org/source/openssl-1.0.1c.tar.gz -o openssl-1.0.1c.tar.gz
    else
        echo "OpenSSL sources are already downloaded..."
    fi

    ALREADY_UNPACKED=`ls -l | grep "^d" | grep "openssl-1.0.1c"`
    if [ -z "$ALREADY_UNPACKED" ]; then
        tar -zxvf openssl-1.0.1c.tar.gz
    else
        echo "OpenSSL sources seems to be already unpacked..."
    fi

    cd openssl-1.0.1c
    perl ./Configure linux-armv4 -march=armv7-a -DSSL_ALLOW_ADH --prefix=$SYSROOT/usr/ no-shared no-asm
    make ${PARALLEL_JOBS}
    make install_sw
    cd ..
else
    echo "OpenSSL building is skipped by config..."
fi

echo "================================================"
echo "= Starting download and compilation of CURL    ="
echo "================================================"

if $BUILD_CURL; then
    IS_DOWNLOADED=`ls -l | grep "curl-7.27.0.tar.gz"`
    if [ -z "$IS_DOWNLOADED" ]; then
        curl -L http://curl.haxx.se/download/curl-7.27.0.tar.gz -o curl-7.27.0.tar.gz
    else
        echo "curl sources are already downloaded..."
    fi

    #set +e

    ALREADY_UNPACKED=`ls -l | grep "^d" | grep "curl-7.27.0"`
    if [ -z "$ALREADY_UNPACKED" ]; then
        tar -zxvf curl-7.27.0.tar.gz
    else
        echo "curl sources seems to be already unpacked..."
    fi

    #set -e

    echo "Building curl..."

    cd curl-7.27.0

    ./configure --host=arm-linux-androideabi --prefix=$SYSROOT/usr/ --with-ssl --with-soname=libcurl --disable-soname-bump
    make ${PARALLEL_JOBS}
    make install

    cd ..
else
    echo "CURL building is skipped by config..."
fi

echo "================================================"
echo "= Starting download and compilation of lua     ="
echo "================================================"

if $BUILD_LUA; then
    IS_DOWNLOADED=`ls -l | grep "lua-android"`
    if [ -z "$IS_DOWNLOADED" ]; then
        git clone https://github.com/henkel/lua-android.git
    else
        echo "lua sources are already downloaded..."
    fi

    cd lua-android/project
    chmod +x ./build.sh
    ./build.sh
    cd ../../
    cp ./lua-android/module/lib/armeabi/liblua-5.1.4* $SYSROOT/usr/lib/
    cp ./lua-android/module/include/*.h  $SYSROOT/usr/include/

    #set +e

    ln -s $SYSROOT/usr/lib/liblua-5.1.4.a $SYSROOT/usr/lib/liblua.a

    #set -e
else
    echo "LUA building is skipped by config..."
fi

echo "================================================"
echo "= Starting download and compilation of boost   ="
echo "================================================"

# if $BUILD_BOOST; then
#     if [ ! -e Boost-for-Android.tar.gz ]; then
#         curl -L https://github.com/sh1r0/Boost-for-Android/archive/master.tar.gz -o Boost-for-Android.tar.gz
#     else
#         echo "boost sources downloaded"
#     fi
#
#     ALREADY_UNPACKED=`ls -l | grep "^d" | grep "Boost-for-Android.tar.gz"`
#     if [ -z "$ALREADY_UNPACKED" ]; then
#         tar -zxvf Boost-for-Android.tar.gz
#     else
#         echo "boost for android sources seems to be already unpacked..."
#     fi
#
#     if [ ! -e boost_1_55_0.tar.bz2 ]; then
#         curl -L http://freefr.dl.sourceforge.net/project/boost/boost/1.55.0/boost_1_55_0.tar.bz2 -o boost_1_55_0.tar.bz2
#     else
#         echo "boost sourcces downloaded 2"
#     fi
#
#     cp ./boost_1_55_0.tar.bz2 ./Boost-for-Android-master/boost_1_55_0.tar.bz2
#
#     cd "./Boost-for-Android-master"
#     # ./build-android.sh ${NDK} --boost=1.55.0 --download --cxx=arm-linux-androideabi-g++ --toolchain=arm-linux-androideabi-4.9
#     ./build-android.sh ${NDK} --boost=1.55.0 --toolchain=arm-linux-androideabi-4.9 --without-libraries=context,coroutine
#
#     cp -R ./build/include/boost-1_55/boost/ ./build/include/
#     cp -R ./build/*  $SYSROOT/usr/
#     cd ..
# else
#     echo "Boost building is skipped by config..."
# fi

# if $BUILD_BOOST; then
#     if [ ! -e Boost-for-Android.tar.gz ]; then
#         curl -L https://github.com/sh1r0/Boost-for-Android/archive/master.tar.gz -o Boost-for-Android.tar.gz
#     else
#         echo "boost sources downloaded"
#     fi
#
#     ALREADY_UNPACKED=`ls -l | grep "^d" | grep "Boost-for-Android-master"`
#     if [ -z "$ALREADY_UNPACKED" ]; then
#         tar -zxvf Boost-for-Android.tar.gz
#     else
#         echo "boost for android sources seems to be already unpacked..."
#     fi
#
#     if [ ! -e boost_1_49_0.tar.bz2 ]; then
#         curl -L http://freefr.dl.sourceforge.net/project/boost/boost/1.49.0/boost_1_49_0.tar.bz2 -o boost_1_49_0.tar.bz2
#     else
#         echo "boost sourcces downloaded 2"
#     fi
#
#     cp ./boost_1_49_0.tar.bz2 ./Boost-for-Android-master/boost_1_49_0.tar.bz2
#
#     cd "./Boost-for-Android-master"
#     # ./build-android.sh ${NDK} --boost=1.55.0 --download --cxx=arm-linux-androideabi-g++ --toolchain=arm-linux-androideabi-4.9
#     # ./build-android.sh ${NDK} --boost=1.54.0 --toolchain=arm-linux-androideabi-4.9 --without-libraries=context,coroutine
#     ./build-android.sh ${NDK} --boost=1.49.0 --toolchain=arm-linux-androideabi-4.9
#
#     cp -R ./build/include/boost-1_49/boost/ ./build/include/
#     cp -R ./build/*  $SYSROOT/usr/
#     cd ..
# else
#     echo "Boost building is skipped by config..."
# fi

if $BUILD_BOOST; then
  IS_DOWNLOADED=`ls -l | grep "Boost-for-Android-Prebuilt"`
  if [ -z "$IS_DOWNLOADED" ]; then
      git clone https://github.com/emileb/Boost-for-Android-Prebuilt.git
  else
      echo "boost sources are already downloaded..."
  fi

  cp -R ./Boost-for-Android-Prebuilt/boost_1_53_0/include/* $SYSROOT/usr/include/
  cp -R ./Boost-for-Android-Prebuilt/boost_1_53_0/armeabi-v7a/lib/* $SYSROOT/usr/lib/

else
    echo "Boost building is skipped by config..."
fi

echo "================================================"
echo "= Starting download and compilation of luabind ="
echo "================================================"

LUA_PATH=$SYSROOT/usr/
export LUA_PATH=$LUA_PATH
set LUA_PATH=$LUA_PATH
export BOOST_PATH=$BOOST_PATH
set BOOST_PATH=$BOOST_PATH
#
# if $BUILD_LUABIND; then
#     IS_DOWNLOADED=`ls -l | grep "luabind-0.9.1.tar.gz"`
#     if [ -z "$IS_DOWNLOADED" ]; then
#         curl -L http://downloads.sourceforge.net/project/luabind/luabind/0.9.1/luabind-0.9.1.tar.gz -o luabind-0.9.1.tar.gz
#     else
#         echo "luabind sources are already downloaded..."
#     fi
#
#     #set +e
#
#     ALREADY_UNPACKED=`ls -l | grep "^d" | grep "luabind-0.9.1"`
#     if [ -z "$ALREADY_UNPACKED" ]; then
#         tar -zxvf luabind-0.9.1.tar.gz
#     else
#         echo "luabind sources seems to be already unpacked..."
#     fi
#
#     #set -e
#
#     cd luabind-0.9.1
#
#     patch -p1 -N -i ../luabind_boost.patch
#     cp ../user-config.jam ./
#     cp ../boost_1_55_0/bjam ./
#
#     LUABIND_CXXFLAGS="-I$NDK/platforms/android-9/arch-arm/usr/include \
#             -I$NDK/sources/cxx-stl/gnu-libstdc++/4.6/include \
#             -I$NDK/sources/cxx-stl/gnu-libstdc++/4.6/libs/armeabi/include \
#             -I$SYSROOT/usr/include/"
#
#     ./bjam --user-config=user-config.jam link=static --prefix=$SYSROOT/usr/ --debug-configuration cxxflags="$LUABIND_CXXFLAGS" install
#
#     cd ..
# else
#     echo "Luabind building is skipped by config..."
# fi


if $BUILD_LUABIND; then
    IS_DOWNLOADED=`ls -l | grep "^d" | grep "luabind"`
    if [ -z "$IS_DOWNLOADED" ]; then
      git clone https://github.com/DennisOSRM/luabind.git
    else
        echo "luabind sources are already downloaded..."
    fi

    mkdir -p luabind/build/
    cd luabind/build/

    cmake -DCMAKE_TOOLCHAIN_FILE=$LOCALDIR/android.toolchain.cmake -DANDROID_STANDALONE_TOOLCHAIN=$TOOLCHAIN -DBUILD_WITH_STANDALONE_TOOLCHAIN=ON -DCMAKE_BUILD_TYPE=Release -DTARGET_CPU="arm" -DCMAKE_INSTALL_PREFIX=$SYSROOT/usr/ ..
    make
    make install

#
#     patch -p1 -N -i ../luabind_boost.patch
#     cp ../user-config.jam ./
#     cp ../boost_1_55_0/bjam ./
#
#     LUABIND_CXXFLAGS="-I$NDK/platforms/android-9/arch-arm/usr/include \
#             -I$NDK/sources/cxx-stl/gnu-libstdc++/4.6/include \
#             -I$NDK/sources/cxx-stl/gnu-libstdc++/4.6/libs/armeabi/include \
#             -I$SYSROOT/usr/include/"
#
#     ./bjam --user-config=user-config.jam link=static --prefix=$SYSROOT/usr/ --debug-configuration cxxflags="$LUABIND_CXXFLAGS" install

    cd ../../
else
    echo "Luabind building is skipped by config..."
fi

# libfreetype6
echo "================================================"
echo "= Starting download and compilation of freetype="
echo "================================================"

if $BUILD_FREETYPE; then
    IS_DOWNLOADED=`ls -l | grep "freetype2-android"`
    if [ -z "$IS_DOWNLOADED" ]; then
        git clone https://github.com/cdave1/freetype2-android.git
    else
        echo "freetype2-android sources are already downloaded..."
    fi

    #set -e

    echo "Building freetype2"

    cd freetype2-android/Android/
    if [ "$platform" = "darwin" ]; then
      sed -i '' -e 's/include_all/include_all ..\/include/g' ./jni/Android.mk
      sed -i '' -e 's/BUILD_STATIC_LIBRARY/BUILD_SHARED_LIBRARY/g' ./jni/Android.mk
    else
      sed -i 's/include_all/include_all ..\/include/g' ./jni/Android.mk
      sed -i 's/BUILD_STATIC_LIBRARY/BUILD_SHARED_LIBRARY/g' ./jni/Android.mk
    fi

    $NDK/ndk-build ${PARALLEL_JOBS}

    cd ../../

    cp ./freetype2-android/Android/libs/armeabi-v7a/libfreetype2-static.so $SYSROOT/usr/lib/libfreetype2.so
    cp ./freetype2-android/Android/libs/armeabi-v7a/libfreetype2-static.so $SYSROOT/usr/lib/libfreetype.so
    cp -R ./freetype2-android/include/* $SYSROOT/usr/include/
else
    echo "Freetype2 building is skipped by config..."
fi

# libopenal

# libogg
echo "================================================"
echo "= Starting download and compilation of libogg  ="
echo "================================================"

if $BUILD_OGG; then
    #set +e

    IS_DOWNLOADED=`ls -l | grep "libogg-1.3.0.tar.gz"`
    if [ -z "$IS_DOWNLOADED" ]; then
        curl -L http://downloads.xiph.org/releases/ogg/libogg-1.3.0.tar.gz -o libogg-1.3.0.tar.gz
    else
        echo "libogg sources are already downloaded..."
    fi

    ALREADY_UNPACKED=`ls -l | grep "^d" | grep "libogg-1.3.0"`
    if [ -z "$ALREADY_UNPACKED" ]; then
        tar -zxvf libogg-1.3.0.tar.gz
    else
        echo "libogg sources seems to be already unpacked..."
    fi

    #http://downloads.xiph.org/releases/ogg/libogg-1.3.0.tar.gz

    #set -e

    cd libogg-1.3.0

    ./configure --host=arm-linux-eabi --disable-tests --disable-examples --with-sysroot=$SYSROOT --prefix=$SYSROOT/usr/ CFLAGS="$CFLAGS"
    make
    make install

    cd ..
else
    echo "LibOGG building is skipped by config..."
fi

# libvorbis
echo "================================================="
echo "= Starting download and compilation of libtremor="
echo "================================================="

if $BUILD_TREMOR; then
    #set +e

    ALREADY_FETCHED=`ls -l | grep "^d" | grep "libtremor"`
    if [ -z "$ALREADY_FETCHED" ]; then
        svn co http://svn.xiph.org/trunk/Tremor/ libtremor -r 19555
    else
        echo "libtremor sources are already downloaded..."
    fi

    #set -e

    cd libtremor

    ./autogen.sh  --host=arm-linux-eabi --disable-examples --with-sysroot=$SYSROOT --prefix=$SYSROOT/usr/ CFLAGS="-march=armv5"

    make
    make install

    cd ..
else
    echo "LibTremor building is skipped by config..."
fi

# libxml2
echo "================================================"
echo "= Starting download and compilation of libxml2 ="
echo "================================================"

if $BUILD_XML; then
    #set +e

    IS_DOWNLOADED=`ls -l | grep "libxml2-2.8.0.tar.gz"`
    if [ -z "$IS_DOWNLOADED" ]; then
        curl -L ftp://xmlsoft.org/libxml2/libxml2-2.8.0.tar.gz -o libxml2-2.8.0.tar.gz
    else
        echo "libxml2 sources are already downloaded..."
    fi

    ALREADY_UNPACKED=`ls -l | grep "^d" | grep "libxml2-2.8.0"`
    if [ -z "$ALREADY_UNPACKED" ]; then
        tar -zxvf libxml2-2.8.0.tar.gz
    else
        echo "libxml2 sources seems to be already unpacked..."
    fi

    #set -e

    echo "Patching libxml2..."

    #set +e
    patch -N ./libxml2-2.8.0/Makefile.in < libxml2-android-build.patch
    #set -e

    echo "Building libxml2..."

    cd libxml2-2.8.0
    ./configure --without-python --host=arm-linux-eabi --with-sysroot=$SYSROOT --prefix=$SYSROOT/usr/ CFLAGS="$CFLAGS"
    make ${PARALLEL_JOBS}
    make install
    cd ..

else
    echo "LibXML2 building is skipped by config..."
fi

# libpng12
echo "================================================"
echo "= Starting download and compilation of libpng  ="
echo "================================================"

if $BUILD_PNG; then
    #set +e

    IS_DOWNLOADED=`ls -l | grep "libpng-1.5.26.tar.gz"`
    if [ -z "$IS_DOWNLOADED" ]; then
        curl -L ftp://ftp.simplesystems.org/pub/libpng/png/src/libpng15/libpng-1.5.26.tar.gz -o libpng-1.5.26.tar.gz
    else
        echo "libpng sources are already downloaded..."
    fi

    ALREADY_UNPACKED=`ls -l | grep "^d" | grep "libpng-1.5.26"`
    if [ -z "$ALREADY_UNPACKED" ]; then
        tar -zxvf libpng-1.5.26.tar.gz
    else
        echo "libpng sources seems to be already unpacked..."
    fi

    #set -e

    cd libpng-1.5.26

    ./configure --host=arm-linux-eabi --with-sysroot=$SYSROOT --prefix=$SYSROOT/usr/
    make ${PARALLEL_JOBS}
    make install
    cd ..
else
    echo "LibPNG building is skipped by config..."
fi

# libwww
echo "================================================"
echo "= Starting download and compilation of libwww  ="
echo "================================================"

if $BUILD_WWW; then
    #set +e

    IS_DOWNLOADED=`ls -l | grep "w3c-libwww-5.4.0.tgz"`
    if [ -z "$IS_DOWNLOADED" ]; then
        curl -L http://www.w3.org/Library/Distribution/w3c-libwww-5.4.0.tgz -o w3c-libwww-5.4.0.tgz
    else
        echo "libwww sources are already downloaded..."
    fi

    ALREADY_UNPACKED=`ls -l | grep "^d" | grep "w3c-libwww-5.4.0"`
    if [ -z "$ALREADY_UNPACKED" ]; then
        tar -zxvf w3c-libwww-5.4.0.tgz
    else
        echo "libwww sources seems to be already unpacked..."
    fi

    #set -e

    cd w3c-libwww-5.4.0

    #set +e
    patch  -N configure ../libwww_arm.patch
    #set -e

    if [ "$platform" = "darwin" ]; then
      sed -i '' -e 's/extern int errno;/\/\/extern int errno;/g' ./Library/src/wwwsys.h
    else
      sed -i 's/extern int errno;/\/\/extern int errno;/g' ./Library/src/wwwsys.h
    fi

    ./configure --host=arm-linux-eabi --with-sysroot=$SYSROOT --prefix=$SYSROOT/usr/ --disable-shared

    make ${PARALLEL_JOBS}
    make install
else
    echo "LibWWW building is skipped by config..."
fi

# libjpeg62

#rm $SYSROOT/usr/lib/libcurl.so
#rm $SYSROOT/usr/lib/libcurl.so.5
#mv $SYSROOT/usr/lib/libcurl.so.5.2.0 libcurl.so

echo "I think we done here"
