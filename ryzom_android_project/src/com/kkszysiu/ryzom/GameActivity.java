/*
===========================================================================

Ryzom for Android project source code
Copyright (C) 2012 Krzysztof Klinikowski <kkszysiu@gmail.com>

===========================================================================
*/

package com.kkszysiu.ryzom;

import android.app.NativeActivity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

public class GameActivity extends NativeActivity {
    static {
        System.loadLibrary("freetype2");
        System.loadLibrary("curl");
//        System.loadLibrary("nelmisc");
//        System.loadLibrary("nel3d");
//        System.loadLibrary("nel_drv_opengles");
//        System.loadLibrary("nelgeorges");
//        System.loadLibrary("nelligo");
//        System.loadLibrary("nellogic");
//        System.loadLibrary("nelnet");
//        System.loadLibrary("nelsound");
//        System.loadLibrary("nelsnd_lowlevel");
//        System.loadLibrary("nelpacs");
//        System.loadLibrary("ryzom_sevenzip");
//        System.loadLibrary("ryzom_clientsheets");
//        System.loadLibrary("ryzom_gameshare");
        System.loadLibrary("ryzom_client");
    }

    private static String TAG = "RyzomNativeActivityJava";
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v(TAG, "Creating RyzomNativeActivityJava");

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                GameActivity.class.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();

        mWakeLock.acquire();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mWakeLock.release();
    }
}
